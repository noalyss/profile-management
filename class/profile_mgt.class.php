<?php
/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief Profile Mgt main class
 */
class Profile_Mgt
{
    function __construct()
    {
        
    }
    function display_profile()
    {
        global $cn;
        $aProfile=$cn->get_array("select * from profile where p_id > 0 order by p_name");
        require_once PROFILEMGT_DIR."/template/profile_mgt-display_profile.php";
    }
    function form_upload()
    {
        require_once PROFILEMGT_DIR."/template/profile_mgt-form_upload.php";
    }
    function upload_file()
    {
        global $cn;
        if (empty($_FILES)) {  throw new Exception(_("Aucun fichier"),100);}
        if ( ! isset ($_FILES['profile_upl'])) {  throw new Exception(_("Aucun fichier"),100);}
        $filename=$_FILES['profile_upl']['tmp_name'];
      
        return $filename;
        
    }
    
    public function validate($pa_Profile)
    {
        
        
        // check keys 
        $aKey=array("type","version","date","profile","profile_menu","profile_mobile");
        foreach ($aKey as $key) {
            if ( ! isset($pa_Profile[$key])) {
                throw new Exception(_("GP1.Fichier invalide"),150);
            }
        }
        if (!is_array($pa_Profile["profile"]) ) {
                throw new Exception(_("GP2.Fichier invalide"),150);
        }
        
       if (!is_array($pa_Profile["profile_menu"]) ) {
                throw new Exception(_("GP3.Fichier invalide"),150);
        }

        if (!is_array($pa_Profile["profile_mobile"]) ) {
                throw new Exception(_("GP4.Fichier invalide"),150);
        }
    }
    public function import_profile($p_filename)
    {
        global $cn;
        if ( !file_exists($p_filename)) {     throw new Exception(_("Aucun fichier"),100);}
        $hfile=fopen( $p_filename,'r');
        $profile_json=fread($hfile, filesize($p_filename));
        $aProfile=json_decode($profile_json,true);
    try {
            $cn->start();
            $this->validate($aProfile) ;
            $cnt=$cn->get_value("select count(*) from profile where lower(p_name) like lower($1)||'%'",
                   [$aProfile["profile"][0]['p_name'] ]);
            $profile_name=$aProfile["profile"][0]['p_name'];
            // first insert into profile
            if ($cnt  > 0 ) {
                $profile_name.=" $cnt";
            }
            $profile_desc=$aProfile["profile"][0]['p_desc']." ".date('Ymd-H:i');
            $new_profile_id=$cn->get_value("
                insert into profile(p_name,p_desc,with_calc,with_direct_form,with_search_card)
                values($1,$2,$3,$4,$5) returning p_id",
                    array(
                        $profile_name,
                        $profile_desc,
                        $aProfile["profile"][0]['with_calc'],
                        $aProfile["profile"][0]['with_direct_form'],
                        $aProfile["profile"][0]['with_search_card']
                        )
                ) ;
            $aKey=["me_code",'me_code_dep',"p_order","p_type_display","pm_default","pm_id_dep"];
            // 2. insert into profile_menu
            foreach ($aProfile['profile_menu'] as $row) {
                $profile_menu=new Profile_Menu_sql($cn);
                foreach ($aKey as $key) {
                    $value=($row[$key]=="")?null:$row[$key];
                    $profile_menu->setp($key,$value);
                }
                $profile_menu->setp("p_id",$new_profile_id);
                $profile_menu->insert();
            }
            // 3. insert into profile_mobile
             $aKey=["me_code","pmo_order","pmo_default"];
              foreach ($aProfile['profile_mobile'] as $row) {
                $profile_mobile=new Profile_Mobile_SQL($cn);
                foreach ($aKey as $key) {
                    $value=($row[$key]=="")?null:$row[$key];
                    $profile_mobile->setp($key,$value);
                }
                $profile_mobile->setp("p_id",$new_profile_id);
                $profile_mobile->insert();
            }
            // Access no warehouse 
            
            // access no management group
            
            
            $cn->commit();
            return $profile_name;
        } catch (\Exception $ex) {
            $cn->rollback();
            throw $ex;
        }

    }
    
    public function export_profile ($p_id) {
        global $cn;
        $a_profile=$cn->get_array("select * from profile where p_id=$1",[$p_id]);
        $a_profile_menu=$cn->get_array("select * from profile_menu where p_id=$1",[$p_id]);
        $a_profile_mobile=$cn->get_array("select * from profile_mobile where p_id=$1",[$p_id]);
        $result=array();
        $result['type']="export_profile";
        $result['version']="1";
        $result["date"]=date('YmdHi');
        $result['profile']=$a_profile;
        $result['profile_menu']=$a_profile_menu;
        $result['profile_mobile']=$a_profile_mobile;
        return json_encode($result);
    }
    
    function run()
    {
        $http=new HttpInput();
        if ( $http->post("upload","string",-1) != -1) {
            if ( empty($_FILES)) {
                echo h2(_("Aucun fichier donné"),'class="error"');
            }
            try {
                global $g_succeed;
                $filename=$this->upload_file();
                $profile_name=$this->import_profile($filename);
                echo '<h2 class="notice">';
                printf (_("Import réussi %s %s"),$g_succeed,$profile_name);
                echo '</h2>';
            } catch(\Exception $ex) {
                echo h2($ex->getMessage(),'class="error"');
            }
        }
        $this->display_profile();
        $this->form_upload();
    }
}
?>
