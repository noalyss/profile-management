<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/*!\file
 * \brief raw file for PDF ewa
 */
 require_once 'profile_mgt_constant.php';

try {
    $act=$http->request("act");
} catch (Exception $ex) {
    die("no action ");
}


/**
 * export selected profile in JSON
 */
if ( $act == "export_profile") {
    try {
        $p_id=$http->get("p_id");
    } catch (Exception $ex) {
        die("no profile_id");
    }
    $profile_mgt=new Profile_Mgt();
    
    if ( DEBUGPROFILEMGT  == 1 ) { 
        header("Content-type: application/json");
    } else {
        header("Content-type: application/bin");
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header('Content-Disposition: attachment;filename="profile'.$p_id.'.json.bin"',FALSE);
        header("Accept-Ranges: bytes");
    }
    echo $profile_mgt->export_profile($p_id);
}