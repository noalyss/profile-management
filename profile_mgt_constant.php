<?php
//This file is part of NOALYSS and is under GPL
//see licence.txt
/**
 *@file
 *Contains all the needed variable for the plugin
 *is name is plugin_name_constant.php
 * You can use some globale variable, especially for the database
 *  connection
 */


global $cn,$gDossier,$g_plugin,$g_access,$g_dir;

$cn=\Dossier::connect();
$http=new \HttpInput();
define("PROFILEMGT_DIR",__DIR__);

$gDossier=\Dossier::id();
$g_plugin=$http->request('plugin_code');
$g_access=$http->request('ac');
require_once PROFILEMGT_DIR.'/class/profile_mgt.class.php';
define ("DEBUGPROFILEMGT",0);