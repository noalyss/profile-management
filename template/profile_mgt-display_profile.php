<?php

/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2021) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief called from Profile_Mgt::display_profile, show a table with existing profile and let you export them
 * parameter $aProfile
 */
global $g_plugin,$g_access,$gDossier;
$p_avertissement=_("Les gestions de dépôt et des groupes de gestion ne sont ni importés ni exportés.");
$p_info = _('Pour corriger le résultat vous devez aller sur C0PROFL')        ;
$url_cfgpro=NOALYSS_URL."/do.php?".http_build_query(["ac"=>"C0PROFL",'gDossier'=>$gDossier]);
?>
<h2>
    <?=_("Liste profils")?>
</h2>
<p>
    <?=$p_avertissement?>
</p>
<p>
    <a href="<?=$url_cfgpro?>"><?=$p_info?></a>
</p>
<table class="result">
    <thead>
    <tr>
    <th>
        <?=_("Nom")?>

    </th>
    <th>
        <?=_("Description")?>
    </th>
    <th>
        
    </th>
    </tr>
    </thead>
    <tbody>
        <?php        
        foreach ($aProfile as $key=>$value) {
        ?>
<tr>
    <td>
        <?=$value['p_name']?>
    </td>
    <td>
        <?=$value['p_desc']?>
    </td>
    <td>
        <?php
            $url="extension.raw.php?".http_build_query(['ac'=>$g_access,'plugin_code'=>$g_plugin,'gDossier'=>$gDossier,
                "p_id"=>$value['p_id'],'act'=>"export_profile"]);
            echo HtmlInput::anchor(_("Télécharger"), "", sprintf("onclick=\"download_document('%s')\"",$url),
                    ' class="line"',_("Télécharger le profil"));
        ?>
    </td>

</tr>
        <?php
        
        } // end loop
        ?>
    </tbody>
</table>